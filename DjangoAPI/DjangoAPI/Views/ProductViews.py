
from django.http import HttpResponse
from DjangoAPI.Models.ProductModel import Product

def index(request):
    return HttpResponse("Welcome to the CB+ test API. Please use the following format: /product/[gtin]")

def product(request, productGtin):
    requestedProduct = Product(productGtin)
    return HttpResponse(requestedProduct.toJson())


import json
import requests

class Product():
    # the URL to use in order to get the product info from the open food facts API
    url = 'https://world.openfoodfacts.org/api/v0/product/'

    # the constructor takes the desired gtin parameter and automatically load the product info from the API
    def __init__(self, gtin):
        self.gtin = gtin
        requestUrl = "{}/{}".format(Product.url, gtin)
        response = requests.get(requestUrl)
        # For successful API call, response code will be 200 (OK)
        if(response.ok):
            # Loading the response data into a dict variable
            jsonData = json.loads(response.content)
            # debug
            print("The response contains {0} properties".format(len(jsonData)))
            print("\n")
            print(json.dumps(jsonData, indent=4, sort_keys=True))
        else:
          # If response code is not ok (200), prints the resulting http error code with description
          response.raise_for_status()

    def toJson(self):
        return json.dumps(self, default=lambda o: o.__dict__)



